import React, { Component } from 'react';
import uuid from 'uuid';

class AddTask extends Component {
    constructor() {
        super();
        this.state = {
            newTask: {}
        }
    }

    handleSubmit(event) {
        if(this.refs.taskText.value === ''){
            alert("Invalid Input : Please Enter some text to input..");
        }
        else{
            this.setState({
                newTask:{
                    id: uuid.v4(),
                    text: this.refs.taskText.value,
                    status: false
                }}, function(){
                this.props.addTask(this.state.newTask);
            });
        }
        event.preventDefault();
    }
    render() {
        return ( 
        <div>
            <h3> Add Task </h3> 
            <form onSubmit = {this.handleSubmit.bind(this)} >
            <input type = "text" ref = "taskText" / >
            <input type = "submit" value = "Submit" / >
            </form> 
        </div>
        );
    }
}

export default AddTask;