import React, { Component } from 'react';
import TaskItem from './TaskItem';

class AllTasks extends Component {
    deleteTask(id){
        this.props.onDelete(id);
    }
    updateTask(event){
        this.props.onChecked(event);
    }
  render() {
    let taskItems;
    if(this.props.taskList){
        taskItems = this.props.taskList.map(item => {
            return (
                <TaskItem onDelete={this.deleteTask.bind(this)} onChecked={this.updateTask.bind(this)} key={item.text} item={item} />
            );
        })
    }
    return (
      <div className="Tasks">
        {taskItems}
      </div>
    );
  }
}

export default AllTasks;