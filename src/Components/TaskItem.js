import React, { Component } from 'react';

class TaskItem extends Component {
    deleteTask(id){
        this.props.onDelete(id);
    }
    updateTask(event){
        console.log(event.target.checked);
        this.props.onChecked(event);
    }
  render() {
    return (
      <li className="Task">
        <input type="checkbox" onChange={this.updateTask.bind(this)} />{this.props.item.text}<a href="#" onClick={this.deleteTask.bind(this, this.props.item.id)}>X</a>
      </li>
    );
  }
}

export default TaskItem;