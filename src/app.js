import React, { Component } from 'react';
import ReactDom from 'react-dom';

import AllTasks from './Components/AllTasks';
import AddTask from './Components/AddTask';

import './css/style.css';

class App extends Component {
  constructor(){
    super();
    this.state = {
      taskList: []
    }
  }
  handleAddTask(task){
    let tasks = this.state.taskList;
    tasks.push(task);
    this.setState({tasks: tasks});
  }
  handleDeleteTask(id){
    let tasks = this.state.taskList;
    let index = tasks.findIndex(x => x.id === id);
    tasks.splice(index, 1);
    this.setState({tasks: tasks});
  }
  handleCheckedTask(event){
    // console.log(event.target.checked);
    // if(event.target.checked){

    // }
    // else{

    // }
  }
  render() {
    return (
      <div className="App">
        <AddTask addTask={this.handleAddTask.bind(this)}/>
        <AllTasks taskList={this.state.taskList} onDelete={this.handleDeleteTask.bind(this)} onChecked={this.handleCheckedTask.bind(this)}/>
      </div>
    );
  }
}

export default App;